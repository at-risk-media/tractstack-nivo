"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomCircle = void 0;
var _react = _interopRequireWildcard(require("react"));
var _tooltip = require("@nivo/tooltip");
var _pie = require("@nivo/pie");
var _theme = require("../services/theme");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
// @ts-ignore

//https://github.com/plouc/nivo/blob/master/packages/bar/src/BarItem.tsx
var CustomCircle = exports.CustomCircle = function CustomCircle(props) {
  var _props$node$data$cate;
  var _useTooltip = (0, _tooltip.useTooltip)(),
    hideTooltip = _useTooltip.hideTooltip;
  var pie = (0, _pie.usePie)({
    data: ((_props$node$data$cate = props.node.data.categories) === null || _props$node$data$cate === void 0 ? void 0 : _props$node$data$cate.map(function (value, id) {
      return {
        id: id,
        value: value,
        hidden: false,
        data: value,
        color: "",
        formattedValue: "".concat(value),
        label: value
      };
    })) || [],
    radius: props.node.size / 2,
    innerRadius: props.node.size / 2 * 0.7,
    sortByValue: true
  });
  var handleMouseEnter = (0, _react.useCallback)(function (event) {
    var _props$onMouseEnter;
    (_props$onMouseEnter = props.onMouseEnter) === null || _props$onMouseEnter === void 0 || _props$onMouseEnter.call(props, props.node, event);
    //showTooltipFromEvent(renderTooltip(), event)
  }, [props /*, showTooltipFromEvent, renderTooltip*/]);
  var handleMouseLeave = (0, _react.useCallback)(function (event) {
    var _props$onMouseLeave;
    (_props$onMouseLeave = props.onMouseLeave) === null || _props$onMouseLeave === void 0 || _props$onMouseLeave.call(props, props.node, event);
    hideTooltip();
  }, [props, hideTooltip]);
  return /*#__PURE__*/_react["default"].createElement("g", {
    transform: "translate(".concat(props.node.x, ",").concat(props.node.y, ")"),
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave
  }, /*#__PURE__*/_react["default"].createElement("circle", {
    r: props.node.size / 2,
    stroke: "rgb(167, 177, 183)",
    strokeWidth: 12
  }), /*#__PURE__*/_react["default"].createElement("circle", {
    r: props.node.size / 2,
    fill: "rgb(227,227,227)",
    stroke: "rgb(227,227,227)",
    strokeWidth: 6
  }), pie.dataWithArc.map(function (datum, i) {
    return /*#__PURE__*/_react["default"].createElement("path", {
      key: i,
      d: pie.arcGenerator(datum.arc) || undefined,
      fill: _theme.oneDarkTheme[i % 16]
    });
  }), props.node.size > 52 && /*#__PURE__*/_react["default"].createElement("text", {
    fill: "black",
    textAnchor: "middle",
    dominantBaseline: "central",
    style: {
      fontSize: 14,
      fontWeight: 800
    }
  }, props.node.value.toString()));
};