"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DetailsPie = void 0;
var _react = _interopRequireDefault(require("react"));
var _pie = require("@nivo/pie");
var _theme = require("../services/theme");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
// @ts-ignore

var DetailsPie = exports.DetailsPie = function DetailsPie(data) {
  return /*#__PURE__*/_react["default"].createElement(_pie.ResponsivePie, {
    data: data.data,
    colors: _theme.oneDarkTheme,
    margin: {
      top: 30,
      right: 10,
      bottom: 30,
      left: 10
    },
    animate: true,
    innerRadius: 0.5,
    padAngle: 0.7,
    cornerRadius: 3,
    activeOuterRadiusOffset: 8,
    borderWidth: 4,
    borderColor: {
      from: "color",
      modifiers: [["darker", 0.2]]
    },
    arcLinkLabelsSkipAngle: 10,
    arcLinkLabelsTextColor: "#333333",
    arcLinkLabelsThickness: 2,
    arcLinkLabelsColor: {
      from: "color"
    },
    arcLabelsSkipAngle: 10,
    arcLabelsTextColor: {
      from: "color",
      modifiers: [["darker", 2]]
    }
  });
};