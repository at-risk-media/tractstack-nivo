"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PaneActivitySwarm = void 0;
var _react = _interopRequireDefault(require("react"));
var _swarmplot = require("@nivo/swarmplot");
var _CustomCircle = require("./CustomCircle");
var _DetailsPie = require("./DetailsPie");
var _theme = require("../services/theme");
var _tooltip = require("../services/tooltip");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); } // @ts-ignore
/*
type ChartProps = {
  valueMin: number,
  valueMax: number,
  eventsValues: number[],
  eventsSizes: number[],
}
const MIN_EVENTS_NODE_SIZE: number = 10;
const MAX_EVENTS_NODE_EXTRA_SPACE_PCT: number = 0.05;
*/

var PaneActivitySwarm = exports.PaneActivitySwarm = function PaneActivitySwarm(payload) {
  var handleClick = payload.handleClick;
  var chartProps = {
    valueMin: 0,
    valueMax: 100,
    eventsValues: [0, 25],
    eventsSizes: [10, 25]
  };
  return /*#__PURE__*/_react["default"].createElement(_swarmplot.ResponsiveSwarmPlot, {
    margin: {
      top: 10,
      right: 30,
      bottom: 80,
      left: 30
    },
    data: payload.data.data,
    groups: payload.data.groups,
    groupBy: "group",
    id: "id",
    value: "engagement",
    valueScale: {
      type: "linear",
      min: chartProps.valueMin,
      max: chartProps.valueMax
    },
    size: {
      key: "events",
      values: chartProps.eventsValues,
      sizes: chartProps.eventsSizes
    },
    spacing: 12,
    enableGridY: false,
    axisTop: null,
    axisRight: null,
    axisLeft: null,
    axisBottom: {
      legend: "Inactive nodes float left. Active nodes float right.",
      legendPosition: "middle",
      legendOffset: 50
    },
    circleComponent: _CustomCircle.CustomCircle,
    layout: "horizontal",
    theme: _theme.theme,
    colors: _theme.oneDarkTheme,
    isInteractive: true,
    useMesh: true,
    tooltip: function tooltip(p) {
      var _p$data, _p$data2, _p$data3;
      return /*#__PURE__*/_react["default"].createElement(_tooltip.NonOverflowTooltipWrapper, {
        point: {
          x: p.x,
          y: p.y
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "text-myblue px-4 pt-4 pb-2 font-main w-full flex flex-col flex-start"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "text-xl font-bold"
      }, p.data.id), /*#__PURE__*/_react["default"].createElement("div", {
        className: "h-36 w-full"
      }, /*#__PURE__*/_react["default"].createElement(_DetailsPie.DetailsPie, {
        data: [{
          id: "skip",
          value: 0
        }, {
          id: "Read",
          value: _typeof(p === null || p === void 0 || (_p$data = p.data) === null || _p$data === void 0 ? void 0 : _p$data.categories) !== "undefined" ? p.data.categories[1] : 0
        }, {
          id: "Glossed",
          value: _typeof(p === null || p === void 0 || (_p$data2 = p.data) === null || _p$data2 === void 0 ? void 0 : _p$data2.categories) !== "undefined" ? p.data.categories[2] : 0
        }, {
          id: "Clicked",
          value: _typeof(p === null || p === void 0 || (_p$data3 = p.data) === null || _p$data3 === void 0 ? void 0 : _p$data3.categories) !== "undefined" ? p.data.categories[3] : 0
        }]
      }))));
    }
    //onMouseEnter={(e) => console.log("enter: " + e.index, e)}
    ,
    onClick: function onClick(e) {
      return handleClick(e);
    }
  });
};