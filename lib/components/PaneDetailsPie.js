"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PaneDetailsPie = void 0;
var _react = _interopRequireDefault(require("react"));
var _pie = require("@nivo/pie");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars

var PaneDetailsPie = exports.PaneDetailsPie = function PaneDetailsPie(data) {
  console.log("PaneDetailsPie", data);
  return /*#__PURE__*/_react["default"].createElement(_pie.ResponsivePie, {
    data: data.data,
    margin: {
      top: 40,
      right: 20,
      bottom: 40,
      left: 20
    },
    animate: true,
    innerRadius: 0.5,
    padAngle: 0.7,
    cornerRadius: 3,
    activeOuterRadiusOffset: 8,
    borderWidth: 4,
    colors: {
      scheme: "category10"
    },
    borderColor: {
      from: "color",
      modifiers: [["darker", 0.2]]
    },
    arcLinkLabelsSkipAngle: 10,
    arcLinkLabelsTextColor: "#333333",
    arcLinkLabelsThickness: 2,
    arcLinkLabelsColor: {
      from: "color"
    },
    arcLabelsSkipAngle: 10,
    arcLabelsTextColor: {
      from: "color",
      modifiers: [["darker", 2]]
    }
  });
};